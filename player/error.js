const Discord = require('discord.js');
module.exports = (client, error, message) => {
    switch (error) {

        case 'NotPlaying':
            const EmbedNotPlaying = new Discord.MessageEmbed()
                .setColor('#5a005a')
                .setTitle(`[Foutmelding]`)
                .setDescription(`Er speelt momenteel geen muziek op deze server!`)
                .setURL('https://minecraft.scouting.nl/')
                .setThumbnail('https://minecraft.scouting.nl/wp-content/uploads/2020/04/Logo_Square.png')
                .setTimestamp();
            message.channel.send(EmbedNotPlaying);
            break;


        case 'NotConnected':
            const EmbedNoChannel = new Discord.MessageEmbed()
                .setColor('#5a005a')
                .setTitle(`[Foutmelding] Je zit niet in een spraakkanaal`)
                .setDescription(`Stap in een spraakkanaal om deze foutmelding te verhelpen`)
                .setURL('https://minecraft.scouting.nl/')
                .setThumbnail('https://minecraft.scouting.nl/wp-content/uploads/2020/04/Logo_Square.png')
                .setTimestamp();
            message.channel.send(EmbedNoChannel);
            break;


        case 'UnableToJoin':
            const EmbedUnableToJoin = new Discord.MessageEmbed()
                .setColor('#5a005a')
                .setTitle(`[Foutmelding]`)
                .setDescription(`Ik kan niet in je spraakkanaal stappen, controleer of mijn permissies wel kloppen!`)
                .setURL('https://minecraft.scouting.nl/')
                .setThumbnail('https://minecraft.scouting.nl/wp-content/uploads/2020/04/Logo_Square.png')
                .setTimestamp();
            message.channel.send(EmbedUnableToJoin);
            break;


        default:
            const EmbedSomething = new Discord.MessageEmbed()
                .setColor('#5a005a')
                .setTitle(`[Foutmelding]`)
                .setDescription(`Er ging iets fout, maar ik weet niet wat`)
                .setURL('https://minecraft.scouting.nl/')
                .setThumbnail('https://minecraft.scouting.nl/wp-content/uploads/2020/04/Logo_Square.png')
                .setTimestamp();
            message.channel.send(EmbedSomething);
    };
};
