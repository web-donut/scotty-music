const Discord = require('discord.js');
module.exports = (client, message, queue, track) => {
        const EmbedEmpty = new Discord.MessageEmbed()
            .setColor('##e71837')
            .setTitle(`Het spraakkanaal is leeg`)
            .setDescription(`Ik ben uit het spraakkanaal gestapt omdat er niemand meer naar mij luistert 😢`)
            .setURL('https://minecraft.scouting.nl/')
            .setThumbnail('https://minecraft.scouting.nl/wp-content/uploads/2020/04/Logo_Square.png')
            .setTimestamp();

        message.channel.send(EmbedEmpty);
};
