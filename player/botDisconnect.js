const Discord = require('discord.js')
module.exports = (client, message, track) => {
        const EmbedDisconnect = new Discord.MessageEmbed()
            .setColor('##00A551')
            .setTitle(`De muziek is gestopt`)
            .setDescription(`De muziek is gestopt omdat ik het spraakkanaal heb verlaten. Wil je weer muziek afspelen? typ dan:
               **/play [nummer]**`)
            .setURL('https://minecraft.scouting.nl/')
            .setThumbnail('https://minecraft.scouting.nl/wp-content/uploads/2020/04/Logo_Square.png')
            .setTimestamp();

        message.channel.send(EmbedDisconnect);
};