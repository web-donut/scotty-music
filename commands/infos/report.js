const Discord = require('discord.js');
const { exec } = require("child_process");
var temperaturen = [];
var cpuuu = [];
var load = [];
var output2;
var output3;
var output4;
var output5;
function thermometer() {
    exec("vcgencmd measure_temp | sed -e 's/temp=//'", (error, stdout, stderr) => {
      output = stdout.split("'")[0]
      temperaturen.push(output)
      //console.log(output);
    });
    var laatsteMeting = temperaturen.length - 1
    //console.log(temperaturen[laatsteMeting]);
    //console.log(String(temperaturen))
    if (temperaturen.length > 9) {
    temperaturen.splice(0, 1)
    }
setTimeout(thermometer, 60000)
}
thermometer()

function cpuuuusage() {
    exec("cpuusage", (error, stdout, stderr) => {
      outputcpu = stdout.split("\n")[0]
      outputcpu2 = (Math.round(outputcpu))
      if (outputcpu2 < 10) {outputcpu2 = ("0" + outputcpu2)
      } else {outputcpu2 = outputcpu2}
      cpuuu.push(outputcpu2)
      //console.log(output);
    });
    var laatsteMetingcpu = cpuuu.length - 1
    //console.log(temperaturen[laatsteMeting]);
    //console.log(String(temperaturen))
    if (cpuuu.length > 9) {
    cpuuu.splice(0, 1)
    }
setTimeout(cpuuuusage, 60000)
}
cpuuuusage()

function memload() {
    exec("load2", (error, stdout, stderr) => {
      outputload = stdout.split("\n")[0]
      outputload2 = (Math.round(outputload * 100) / 100.0)
      load.push(outputload2)
      //console.log(output);
    });
    var laatsteMetingload = load.length - 1
    //console.log(temperaturen[laatsteMeting]);
    //console.log(String(temperaturen))
    if (load.length > 9) {
    load.splice(0, 1)
    }
setTimeout(memload, 60000)
}
memload()



module.exports = {
    name: 'rapport',
    aliases: [],
    category: 'Leiding',
    utilisation: '{prefix}rapport',

    execute(client, message, clientuser) {
      exec("cpuusage", (error, stdout, stderr) => {
          if (error) {
              output4 = (`error: ${error.message}`);
              return;
          }
          if (stderr) {
              output4 = (`stderr: ${stderr}`);
              return;
          }
          output4 = stdout.split("\n")[0];

      exec("load2", (error, stdout, stderr) => {
          if (error) {
              output5 = (`error: ${error.message}`);
              return;
          }
          if (stderr) {
              output5 = (`stderr: ${stderr}`);
              return;
          }
          output5 = stdout.split("\n")[0];



      function streepjesMaker(x) {
        var temperatuur = Math.round(x);
        var hoevaak = ((temperatuur - 40) / 1.142857);
        var streepjes = []
        if (temperatuur <= 40) {
            streepjes.push("░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░")

        } else {
        for (y = 0; y <= hoevaak; y++) {
          streepjes.push("█")
        }
        for (z = 0; z < (34 - hoevaak); z++) {
          streepjes.push("░")
        }}
        return (streepjes.join(""))
      }
      function schema () {
      bericht = []
      for (x = 0; x < temperaturen.length; x++) {
      bericht.push('\n`' + temperaturen[x] + '`°C ' + "....|40°C|" + streepjesMaker(temperaturen[x]) + '|80°C')
      }
      return (bericht.join(""))
      }

      function streepjesCpu(x) {
           var cpuusage = Math.round(x);
           var hoevaakcpu = (cpuusage / 2.857142);
           var streepjescpu = []
           if (cpuusage <= 0) {
                streepjescpu.push("░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░")

         } else {
           for (y = 0; y <= hoevaakcpu; y++) {
            streepjescpu.push("█")
            }
            for (z = 0; z < (34 - hoevaakcpu); z++) {
            streepjescpu.push("░")
             }}
      return (streepjescpu.join(""))
      }
      function schemacpu () {
      berichtcpu = []
      for (x = 0; x < cpuuu.length; x++) {
      berichtcpu.push('\n`' + cpuuu[x] + '`% ' + "............|0%|" + streepjesCpu(cpuuu[x]) + '|100%')
      }
      return (berichtcpu.join(""))
      }

      function streepjesLoad(x) {
           var cpuload = x;
           var hoevaakload = (cpuload * 8.75);
           var streepjesload = []
           if (cpuload = 0) {
                streepjesload.push("░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░")

         } else {
           for (y = 0; y <= hoevaakload; y++) {
            streepjesload.push("█")
            }
            for (z = 0; z < (34 - hoevaakload); z++) {
            streepjesload.push("░")
             }}
      return (streepjesload.join(""))
      }
      function schemaload () {
      berichtload = []
      for (x = 0; x < load.length; x++) {
      berichtload.push('\n`' + load[x].toFixed(2) + '`' + ".................|0|" + streepjesLoad(load[x]) + '|4')
      }
      return (berichtload.join(""))
      }

      exec("vcgencmd measure_temp | sed -e 's/temp=//'", (error, stdout, stderr) => {
          if (error) {
              output2 = (`error: ${error.message}`);
              return;
          }
          if (stderr) {
              output2 = (`stderr: ${stderr}`);
              return;
          }
          output2 = stdout.split("'")[0];

        // Check if they have one of many roles
        if(message.member.roles.cache.some(r=>["Leiding", "Staf", "Developer"].includes(r.name)) ) {
          var statusupdate;
            if (output5 >= 3.2) {statusupdate = `⚠️**De bots kunnen uitvallen met deze Uptime Average Load (${output5})**⚠️`
          } else {
            if (output2 >= 75) {statusupdate = `⚠️**De bots kunnen uitvallen met deze Temperatuur (${output2})**⚠️`}
            else {statusupdate= `Het gaat goed met de bots!`}};
          // has one of the roles
          var kanaal;//`${message.guild.voice} !== undefined ? ${message.guild.voice.channel.name} : null`;
          if(!message.guild.me.voice.channel) {kanaal = 'Nee'}
          if(message.guild.me.voice.channel) { kanaal = `${message.guild.voice.channel.name}`};

            message.channel.send({
                embed: {
                    color: '#5a005a',
                    author: { name: 'Volledig rapport' },
                    fields: [
                        { name: 'Ping', value: `${client.ws.ping}ms`, inline:true },
                        { name: 'Kanalen', value: `${client.voice.connections.size}`, inline:true },
                        { name: 'Servers', value: `${client.guilds.cache.size}`, inline:true },
                        { name: 'Gebruikers', value: `${client.users.cache.size}`, inline:true },
                        { name: 'Aanwezig?', value: kanaal, inline:true },
                        { name: 'Huidige temp:', value: (output2 + '°C'), inline:true },
                        { name: 'CPU activiteit:', value: (output4 + '%'), inline:true },
                        { name: 'Uptime Load (5m):', value: (output5), inline:true },
                        { name: 'Temperaturen (afgelopen 10 minuten):', value: schema(), inline:false},
                        { name: 'Cpu activiteit (afgelopen 10 minuten):', value: schemacpu(), inline:false},
                        { name: '(5m)Uptime Load Average (afgelopen 10 minuten):', value: schemaload(), inline:false},
                    ],
                    timestamp: new Date(),
                    description: statusupdate,
                },
            });

        } else {
        message.channel.send({
                        embed: {
                            color: '#e71837',
                            author: { name: 'Geen rechten' },
                            timestamp: new Date(),
                            description: `Je hebt de rechten niet om **/rapport** uit te mogen voeren`,
                        },
                    })};          // has none of the roles
})})})
        client.user.setActivity(client.config.discord.activity);


      }
    };
