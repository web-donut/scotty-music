const Discord = require('discord.js');
module.exports = {
    name: 'pause',
    aliases: [],
    category: 'Music',
    utilisation: '{prefix}pause',

    execute(client, message) {
        const EmbedNotInChannel = new Discord.MessageEmbed()
            .setColor('#e71837')
        	.setTitle(`Je zit niet in een spraakkanaal`)
        	.setDescription(`Join eerst een spraakkanaal en start een liedje met **/play [nummer]**`)
        	.setURL('https://minecraft.scouting.nl/')
        	.setThumbnail('https://minecraft.scouting.nl/wp-content/uploads/2020/04/Logo_Square.png')
        	.setTimestamp();
        if (!message.member.voice.channel) return message.channel.send(EmbedNotInChannel);


        const EmbedNoSongInQueue = new Discord.MessageEmbed()
            .setColor('#fc9303')
            .setTitle(`De wachtrij is leeg!`)
            .setDescription(`Er zitten nog geen liedjes in de wachtrij. Voeg liedjes toe met
                **/play [nummer]**`)
            .setURL('https://minecraft.scouting.nl/')
            .setThumbnail('https://minecraft.scouting.nl/wp-content/uploads/2020/04/Logo_Square.png')
            .setTimestamp();
        if (!client.player.getQueue(message)) return message.channel.send(EmbedNoSongInQueue);


        const EmbedNotInSameChannel = new Discord.MessageEmbed()
            .setColor('#e71837')
            .setTitle(`Je zit niet in het goede spraakkanaal`)
            .setDescription(`Als je de muziek wil pauzeren moet je in het volgende spraakkanaal zitten: **${message.guild.voice.channel.name}**`)
            .setURL('https://minecraft.scouting.nl/')
            .setThumbnail('https://minecraft.scouting.nl/wp-content/uploads/2020/04/Logo_Square.png')
            .setTimestamp();
        if (message.guild.me.voice.channel && message.member.voice.channel.id !== message.guild.me.voice.channel.id) return message.channel.send(EmbedNotInSameChannel);


        const EmbedAlreadyPaused = new Discord.MessageEmbed()
            .setColor('#e71837')
            .setTitle(`De muziek is al gepauzeerd`)
            .setDescription(`Je kan de muziek weer starten door **/resume** te typen`)
            .setURL('https://minecraft.scouting.nl/')
            .setThumbnail('https://minecraft.scouting.nl/wp-content/uploads/2020/04/Logo_Square.png')
            .setTimestamp();
        if (client.player.getQueue(message).paused) return message.channel.send(EmbedAlreadyPaused);


        client.player.pause(message);
        const EmbedPause = new Discord.MessageEmbed()
            .setColor('#00A551')
            .setTitle(`De muziek is gepauzeerd`)
            .setDescription(`Je kan de muziek weer starten door **/resume** te typen`)
            .setURL('https://minecraft.scouting.nl/')
            .setThumbnail('https://minecraft.scouting.nl/wp-content/uploads/2020/04/Logo_Square.png')
            .setTimestamp();
        message.channel.send(EmbedPause);
    },
};