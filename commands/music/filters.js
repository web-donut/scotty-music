const Discord = require('discord.js');
module.exports = {
    name: 'filters',
    aliases: ['fil'],
    category: 'Leiding',
    utilisation: '{prefix}filters',

    execute(client, message, args) {
      if(message.member.roles.cache.some(r=>["Leiding", "Staf", "Developer"].includes(r.name)) ) {
        const EmbedNotInChannelSkip = new Discord.MessageEmbed()
                    .setColor('#e71837')
                	.setTitle(`Je zit niet in een spraakkanaal`)
                	.setDescription(`Als je de filters wil inzien moet je een spraakkanaal joinen`)
                	.setURL('https://minecraft.scouting.nl/')
                	.setThumbnail('https://minecraft.scouting.nl/wp-content/uploads/2020/04/Logo_Square.png')
                	.setTimestamp();
        if (!message.member.voice.channel) return message.channel.send(EmbedNotInChannelSkip);

        const EmbedNotInSameChannelSkip = new Discord.MessageEmbed()
                    .setColor('#e71837')
                	.setTitle(`Je zit niet in het goede spraakkanaal`)
                	.setDescription(`Als je de filters wil inzien moet je in **${message.guild.me.voice.channel}** zitten`)
                	.setURL('https://minecraft.scouting.nl/')
                	.setThumbnail('https://minecraft.scouting.nl/wp-content/uploads/2020/04/Logo_Square.png')
                	.setTimestamp();
        if (message.guild.me.voice.channel && message.member.voice.channel.id !== message.guild.me.voice.channel.id) return message.channel.send(EmbedNotInSameChannelSkip);

        const EmbedNoSongInQueue = new Discord.MessageEmbed()
            .setColor('#fc9303')
            .setTitle(`De wachtrij is leeg!`)
            .setDescription(`Er zitten nog geen liedjes in de wachtrij. Voeg liedjes toe met
                **/play [nummer]**`)
            .setURL('https://minecraft.scouting.nl/')
            .setThumbnail('https://minecraft.scouting.nl/wp-content/uploads/2020/04/Logo_Square.png')
            .setTimestamp();
        if (!client.player.getQueue(message)) return message.channel.send(EmbedNoSongInQueue);


        const filtersStatuses = [[], []];

        client.filters.forEach((filterName) => {
            const array = filtersStatuses[0].length > filtersStatuses[1].length ? filtersStatuses[1] : filtersStatuses[0];
            array.push(filterName.charAt(0).toUpperCase() + filterName.slice(1) + " : " + (client.player.getQueue(message).filters[filterName] ? client.emotes.success : client.emotes.off));
        });

        message.channel.send({
            embed: {
                color: '#00A551',
                fields: [
                    { name: 'Filters', value: filtersStatuses[0].join('\n'), inline: true },
                    { name: '** **', value: filtersStatuses[1].join('\n'), inline: true },
                ],
                timestamp: new Date(),
                description: `Een lijst met alle filters.\nGebruik \`${client.config.discord.prefix}filter\` om een filter toe te voegen aan een liedje`,
            },
        });
    } else {
        message.channel.send({
                        embed: {
                            color: '#e71837',
                            author: { name: 'Geen rechten' },
                            timestamp: new Date(),
                            description: `Je hebt de rechten niet om **/filters** uit te mogen voeren`,
                        },
                    })};          // has none of the roles,
    },
};
